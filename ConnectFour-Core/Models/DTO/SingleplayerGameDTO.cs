﻿namespace ConnectFour_Core.Models.DTO
{
    using System;
    using System.Collections.Generic;

    public class SingleplayerGameDTO
    {
        public Guid GameId { get; set; }
        public List<Player> Players { get; set; }
        public bool Finished { get; set; }
        public Player Winner { get; set; }
        public Player CurrentPlayerTurn { get; set; }
        public BoardDTO Board { get; set; }

        public static SingleplayerGameDTO GameToGameDTO(Game game)
        {
            return new SingleplayerGameDTO
            {
                GameId = game.Id,
                Players = game.Players,
                CurrentPlayerTurn = game.CurrentPlayerTurn,
                Board = BoardDTO.BoardToBoardDTO(game.Board),
                Finished = game.Finished,
                Winner = game.Winner,
            };
        }
    }

}
