﻿namespace ConnectFour_Core.Models.DTO
{
    using ConnectFour_Core.Extensions;

    public class BoardDTO
    {
        public byte Width { get; set; }
        public byte Height { get; set; }
        public bool?[][] Board { get; set; }

        public static BoardDTO BoardToBoardDTO(Board board)
        {
            var newBoard = board.Grid.ToJaggedArray();

            return new BoardDTO
            {
                Width = board.Width,
                Height = board.Height,
                Board = newBoard
            };
        }
    }
}
