﻿namespace ConnectFour_Core.Models.DTO
{
    using System;
    using System.Collections.Generic;

    public class MultiplayerGameDTO
    {
        public Guid GameId { get; set; }
        public List<Player> Players { get; set; }
        public bool InProgress { get; set; }
        public bool Finished { get; set; }
        public Player Winner { get; set; }
        public Player CurrentPlayerTurn { get; set; }
        public BoardDTO Board { get; set; }

        public static MultiplayerGameDTO GameToGameDTO(Game game)
        {
            return new MultiplayerGameDTO
            {
                GameId = game.Id,
                Players = game.Players,
                InProgress = game.InProgress,
                CurrentPlayerTurn = game.CurrentPlayerTurn,
                Board = BoardDTO.BoardToBoardDTO(game.Board),
                Finished = game.Finished,
                Winner = game.Winner,
            };
        }
    }
}
