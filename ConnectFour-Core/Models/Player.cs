﻿namespace ConnectFour_Core.Models
{
    public class Player
    {
        public string ConnectionId { get; set; } /// Unique connection identifier for multiplayer games
        public bool IdInGame { get; set; } ///There can only be 2 players in a game, so a bool is used. For player 1: false/0, and player 2: true/1
        public string Name { get; set; }

        public Player(bool gameId, string name)
        {
            IdInGame = gameId;
            Name = name;
        }

        public Player(string connnectionId)
        {
            ConnectionId = connnectionId;
        }
    }
}
