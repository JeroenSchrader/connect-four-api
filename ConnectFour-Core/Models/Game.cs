﻿namespace ConnectFour_Core.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class Game
    {
        public Guid Id { get; } ///Unique game identifier
        public Board Board { get; private set; }
        public Player Player1 { get; private set; }
        public Player Player2 { get; private set; }
        public List<Player> Players { get; private set; }
        public bool InProgress { get; set; } /// Used in multiplayer to find games that haven't started yet

        public Player Winner { get => Players.FirstOrDefault(player => player.IdInGame == Board.GetWinner()); }
        private bool Draw { get => Winner == null && Board.IsFull; }
        public bool Finished { get => Draw || Winner != null; }

        public Player CurrentPlayerTurn { get => Finished ? null : Players.First(player => player.IdInGame == _playerTurn); }
        private bool _playerTurn;

        /// <summary>
        /// Initialize a singleplayer game with 6*7 board and two players
        /// TODO: Add custom game options
        /// </summary>
        public Game(List<Player> players)
        {
            Id = Guid.NewGuid();
            Player1 = players.First(player => player.IdInGame == false);
            Player2 = players.First(player => player.IdInGame == true);
            Players = new List<Player> { Player1, Player2 };

            Board = new Board();
            InProgress = true;

            _playerTurn = false; ///Player 1(false/0) starts
        }

        /// <summary>
        /// Initialize a multiplayer game with 6*7 board
        /// TODO: Add custom game options
        /// </summary>
        public Game(Player gameCreator)
        {
            Id = Guid.NewGuid();
            Player1 = gameCreator;
            InProgress = false;
        }

        /// <summary>
        /// Adds the second player to a multiplayer game and starts the game
        /// </summary>
        /// <param name="player2">The second player joining an existing game</param>
        /// <returns>True if succesfully joined, false if game already started</returns>
        public bool AddPlayer2(Player player2)
        {
            if (Players != null || InProgress)
            {
                ///Game already running
                return false;
            }

            Player2 = player2;
            Players = new List<Player> { Player1, Player2 };

            CoinToss();

            Board = new Board();
            InProgress = true;

            return true;
        }

        /// <summary>
        /// Adds a chip to a specified column for the current player
        /// </summary>
        /// <param name="columnIndex">The index of the column to insert the chip into</param>
        /// <returns>true if the chip was added, false if something went wrong</returns>
        public bool AddChip(byte columnIndex)
        {
            if (Finished)
            {
                return false;
            }

            bool succes = Board.AddChip(_playerTurn, columnIndex);
            if (succes)
            {
                _playerTurn = !_playerTurn;
            }
            return succes;
        }

        /// <summary>
        /// Randomly decide who starts first
        /// </summary>
        private void CoinToss()
        {
            var result = new Random().Next(2);
            if (result == 0)
            {
                Player1.IdInGame = false;                
                Player2.IdInGame = true;                
            }
            else
            {
                Player1.IdInGame = true;
                Player2.IdInGame = false;
            }
            _playerTurn = false; 
        }
    }
}
