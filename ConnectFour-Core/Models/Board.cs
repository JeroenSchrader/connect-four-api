﻿namespace ConnectFour_Core.Models
{
    using System;
    using ConnectFour_Core.Extensions;

    public class Board
    {
        /// <summary>
        /// The grid coordinate system works as follows:
        /// <para>[0,0] [0,1] [0,2] [0,3] [0,4] [0,5] [0,6]</para>
        /// <para>[1,0] [1,1] [1,2] [..] </para>
        /// <para>[2,0] [2,1] [2,2] [..] </para> 
        /// <para>[3,0]  [..]  [..]  [..]</para>
        /// <para>[4,0]  [..]  [..]  [..]</para>
        /// <para>[5,0]  [..]  [..]  [..]</para>
        /// <para>That means the first chip added at column 0 will be situated at [5,0] in a standard board of 6*7 cells</para>
        /// </summary>
        public bool?[,] Grid { get; private set; }
        public byte Width { get; private set; }
        public byte Height { get; private set; }

        public bool IsFull { get => _chipCount == Width * Height; }
        private byte _chipCount;
        private sbyte[] columnPointers { get; set; }

        /// <summary>
        /// Initialize a standard 6*7 connect-four board
        /// </summary>
        public Board()
        {
            Width = 7;
            Height = 6;
            Reset();
        }

        /// <summary>
        /// Create a new board with a specified width and height. NOTE: width and/or height can be a maximimum of 15, and have to be a minumum of 6
        /// </summary>
        /// <param name="width">Width of the board</param>
        /// <param name="height">Height of the board</param>
        public Board(byte width, byte height)
        {
            if (width > 15 || height > 15 || width < 6 || height < 6)
            {
                throw new ArgumentException($"width and/or height not valid. width ({width}) and height ({height}) can be a maximum of 15 and have to be a minimum of 6");
            }

            Width = width;
            Height = height;
            Reset();
        }

        /// <summary>
        /// Resets the board to it's initial state
        /// </summary>
        public void Reset()
        {
            _chipCount = 0;
            Grid = new bool?[Height, Width];
            columnPointers = new sbyte[Width];
            InitializeColumnPointers();
        }

        /// <summary>
        /// Adds a chip from a player to a specified column within the board
        /// </summary>
        /// <param name="playerGameId">The ID of the player within this game. Can only be 0 or 1 (player 1 or 2)</param>
        /// <param name="columnIndex">The index of the column in which the chip should be inserted</param>
        /// <returns>true when the method succeeded, false if something went wrong</returns>
        public bool AddChip(bool playerGameId, byte columnIndex)
        {
            if (columnIndex >= Width)
            {
                //Column is not within the board dimensions
                return false;
            }

            sbyte columnHeightIndex = columnPointers[columnIndex];
            if (columnHeightIndex < 0)
            {
                //Column is already full
                return false;
            }

            Grid[columnHeightIndex, columnIndex] = playerGameId;
            columnPointers[columnIndex]--;
            _chipCount++;
            return true;
        }

        /// <summary>
        /// Checks if there's a winner (someone connected 4 chips together)
        /// </summary>
        /// <returns>
        ///     - false (0) if player 1 won.
        ///     - true (1) if player 2 won.        
        ///     - null if no one won yet
        /// </returns>
        public bool? GetWinner()
        {
            bool? currentPlayerCheck = null;
            //Vertical check
            for (int i = 0; i < Width; i++)
            {
                for (int j = 0; j < Height - 3; j++)
                {
                    currentPlayerCheck = Grid[j, i].GetValueOrNull();
                    if (currentPlayerCheck == null)
                    {
                        continue;
                    }
                    if (Grid[j + 1, i].GetValueOrNull() == currentPlayerCheck && Grid[j + 2, i].GetValueOrNull() == currentPlayerCheck && Grid[j + 3, i].GetValueOrNull() == currentPlayerCheck)
                    {
                        return currentPlayerCheck;
                    }
                }
            }

            //Horizontal check
            for (int i = 0; i < Height; i++)
            {
                for (int j = 0; j < Width - 3; j++)
                {
                    currentPlayerCheck = Grid[i, j].GetValueOrNull();
                    if (currentPlayerCheck == null)
                    {
                        continue;
                    }
                    if (Grid[i, j + 1].GetValueOrNull() == currentPlayerCheck && Grid[i, j + 2].GetValueOrNull() == currentPlayerCheck && Grid[i, j + 3].GetValueOrNull() == currentPlayerCheck)
                    {
                        return currentPlayerCheck;
                    }
                }
            }

            //Ascending diagonal check
            for (int i = 0; i < Width - 3; i++)
            {
                for (int j = 3; j < Height; j++)
                {
                    currentPlayerCheck = Grid[j, i].GetValueOrNull();
                    if (currentPlayerCheck == null)
                    {
                        continue;
                    }
                    if (Grid[j - 1, i + 1].GetValueOrNull() == currentPlayerCheck && Grid[j - 2, i + 2].GetValueOrNull() == currentPlayerCheck && Grid[j - 3, i + 3].GetValueOrNull() == currentPlayerCheck)
                    {
                        return currentPlayerCheck;
                    }
                }
            }

            //Descending diagonal check
            for (int i = Width - 4; i >= 0; i--)
            {
                for (int j = 0; j < Height - 3; j++)
                {
                    currentPlayerCheck = Grid[j, i].GetValueOrNull();
                    if (currentPlayerCheck == null)
                    {
                        continue;
                    }
                    if (Grid[j + 1, i + 1].GetValueOrNull() == currentPlayerCheck && Grid[j + 2, i + 2].GetValueOrNull() == currentPlayerCheck && Grid[j + 3, i + 3].GetValueOrNull() == currentPlayerCheck)
                    {
                        return currentPlayerCheck;
                    }
                }
            }

            //No winner
            return null;
        }

        /// <summary>
        /// Sets the pointers for each column in the Grid to the bottom slot
        /// </summary>
        private void InitializeColumnPointers()
        {
            for (int i = 0; i < columnPointers.Length; i++)
            {
                columnPointers[i] = (sbyte)(Height - 1);
            }
        }
    }
}
