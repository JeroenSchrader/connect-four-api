﻿namespace ConnectFour_Core.Services.Interfaces
{
    using System;
    using System.Collections.Generic;
    using ConnectFour_Core.Models;

    public interface IGameService
    {
        /// <summary>
        /// Creates a new game with specified players and returns the unique identifier (Guid) of the newly created game
        /// </summary>
        /// <param name="players">The players that will participate in game</param>
        /// <returns>An unique identifier of the game</returns>
        Guid CreateNewGame(List<Player> players);

        /// <summary>
        /// Creates a new multiplayer game. Gamecreator will have to wait until someone else joins his game
        /// </summary>
        /// <param name="gameCreator">The player who created the game.</param>
        /// <returns>A unique identifier of the game</returns>
        Guid CreateNewGame(Player gameCreator);

        Game GetGameById(Guid gameId);

        List<Game> GetAllGames();

        List<Game> GetAllRunningGames();

        /// <summary>
        /// Removes the game with specified Id from the list of running games. Return true if succesfully removed.
        /// </summary>
        /// <param name="gameId">The unique identifier of the game</param>
        /// <returns>true if succesfully removed, false if not</returns>
        bool RemoveGameById(Guid gameId);
    }
}
