﻿using System;
using ConnectFour_Core.Models;

namespace ConnectFour_Core.Services.Interfaces
{
    public interface IConnectFourService
    {
        /// <summary>
        /// Adds a chip to a specified column within game with a certain gameId and return the new game state
        /// </summary>
        /// <param name="gameId">The unique identifier of the game to add the chip to</param>
        /// <param name="columnIndex">The column in which the chip has to be inserted</param>
        /// <returns>The new game state</returns>
        Game AddChip(Guid gameId, byte columnIndex);

        /// <summary>
        /// Adds a chip to a specified column within a certain game and return the new game state
        /// </summary>
        /// <param name="game">The game to add the chip to</param>
        /// <param name="columnIndex">The column in which the chip has to be inserted</param>
        /// <returns>The new game state</returns>
        Game AddChip(Game game, byte columnIndex);
    }
}
