﻿namespace ConnectFour_Core.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ConnectFour_Core.Models;
    using ConnectFour_Core.Services.Interfaces;

    public class GameService : IGameService
    {
        private readonly List<Game> _games;

        public GameService()
        {
            _games = new List<Game>();
        }

        public Guid CreateNewGame(List<Player> players)
        {
            var game = new Game(players);
            _games.Add(game);
            return game.Id;
        }

        public Guid CreateNewGame(Player gameCreator)
        {
            var game = new Game(gameCreator);
            _games.Add(game);
            return game.Id;
        }

        public Game GetGameById(Guid gameId)
        {
            return _games.FirstOrDefault(game => game.Id == gameId);
        }

        public List<Game> GetAllGames()
        {
            return _games;
        }

        public List<Game> GetAllRunningGames()
        {
            return _games.Where(game => game.InProgress).ToList();
        }

        public bool RemoveGameById(Guid gameId)
        {
            var game = GetGameById(gameId);

            if (game != null)
            {
                _games.Remove(game);
                return true;
            }

            return false;
        }
    }
}
