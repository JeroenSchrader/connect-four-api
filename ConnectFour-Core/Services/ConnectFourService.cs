﻿namespace ConnectFour_Core.Services
{
    using System;
    using ConnectFour_Core.Models;
    using ConnectFour_Core.Services.Interfaces;
    using Microsoft.Extensions.Logging;

    public class ConnectFourService : IConnectFourService
    {
        private readonly IGameService _gameService;
        private readonly ILogger<ConnectFourService> _logger;

        public ConnectFourService(IGameService gameService, ILogger<ConnectFourService> logger)
        {
            _gameService = gameService;
            _logger = logger;
        }

        public Game AddChip(Guid gameId, byte columnIndex)
        {
            var game = _gameService.GetGameById(gameId);
            if (game == null)
            {
                _logger.LogCritical($"Used tried to add a chip to a non-existing game. Game with gameId {gameId} is not found");
                throw new ArgumentException($"Game with gameId {gameId} is not found.");
            }
            var succes = game.AddChip(columnIndex);
            if (!succes)
            {
                _logger.LogInformation("User tried to add a chip but it did not work. Either the chosen column is already full or the columnIndex is invalid");
            }
            return game;
        }

        public Game AddChip(Game game, byte columnIndex)
        {
            if (game == null)
            {
                _logger.LogCritical($"Used tried to add a chip to a non-existing game");
                throw new ArgumentException($"Game not found.");
            }
            var succes = game.AddChip(columnIndex);
            if (!succes)
            {
                _logger.LogInformation("User tried to add a chip but it did not work. Either the chosen column is already full or the columnIndex is invalid");
            }
            return game;
        }
    }
}
