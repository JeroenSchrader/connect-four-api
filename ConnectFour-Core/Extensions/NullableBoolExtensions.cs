﻿namespace ConnectFour_Core.Extensions
{
    public static class NullableBoolExtensions
    {
        public static bool? GetValueOrNull(this bool? value)
        {
            if (value.HasValue)
            {
                return value.Value;
            }
            else
            {
                return null;
            }
        }
    }
}
