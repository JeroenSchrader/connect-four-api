﻿namespace ConnectFour_Core.Extensions
{
    public static class ArrayExtensions
    {
        /// <summary>
        /// Converts a two-dimensional array to a jagged array. Reason for doing this is because a two-dimensional array can not be serialized by System.Json
        /// </summary>
        /// <typeparam name="T">The type of two-dimensional array</typeparam>
        /// <param name="twoDimensionalArray">The two-dimensional array itself</param>
        /// <returns>A jagged array of the same type</returns>
        public static T[][] ToJaggedArray<T>(this T[,] twoDimensionalArray)
        {
            int rowsFirstIndex = twoDimensionalArray.GetLowerBound(0);
            int rowsLastIndex = twoDimensionalArray.GetUpperBound(0);
            int numberOfRows = rowsLastIndex + 1;

            int columnsFirstIndex = twoDimensionalArray.GetLowerBound(1);
            int columnsLastIndex = twoDimensionalArray.GetUpperBound(1);
            int numberOfColumns = columnsLastIndex + 1;

            T[][] jaggedArray = new T[numberOfRows][];
            for (int i = rowsFirstIndex; i <= rowsLastIndex; i++)
            {
                jaggedArray[i] = new T[numberOfColumns];

                for (int j = columnsFirstIndex; j <= columnsLastIndex; j++)
                {
                    jaggedArray[i][j] = twoDimensionalArray[i, j];
                }
            }
            return jaggedArray;
        }
    }
}
