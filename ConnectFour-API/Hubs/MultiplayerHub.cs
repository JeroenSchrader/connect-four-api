﻿namespace ConnectFour_API.Hubs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using ConnectFour_Core.Models;
    using ConnectFour_Core.Models.DTO;
    using ConnectFour_Core.Services.Interfaces;
    using Microsoft.AspNetCore.SignalR;
    using Microsoft.Extensions.Logging;

    public interface IGameClient
    {
        Task SendNewBoardState(MultiplayerGameDTO gameState);
        Task SendPlayerDisconnected();
    }

    public class MultiplayerHub : Hub<IGameClient>
    {
        private readonly IGameService _gameService;
        private readonly IConnectFourService _connectFourService;
        private readonly ILogger<MultiplayerHub> _logger;

        public MultiplayerHub(IGameService gameService, IConnectFourService connectFourService, ILogger<MultiplayerHub> logger)
        {
            _gameService = gameService;
            _connectFourService = connectFourService;
            _logger = logger;
        }

        /// <summary>
        /// Runs when client sends request to add a chip to a specified column.
        /// </summary>
        /// <param name="columnIndex">The column the client want to add a chip to</param>        
        public async Task AddChip(string playerName, byte columnIndex)
        {
            var connectionId = Context.ConnectionId;
            var game = _gameService.GetAllRunningGames().FirstOrDefault(game => game.Player1.ConnectionId == connectionId || game.Player2.ConnectionId == connectionId);
            if(game.CurrentPlayerTurn.Name != playerName)
            {
                _logger.LogInformation($"Player with name {playerName} tried to make a move when it was not his turn.");
                return;
            }

            if (game != null)
            {
                var newGameState = _connectFourService.AddChip(game, columnIndex);
                await Clients.Group(game.Id.ToString()).SendNewBoardState(MultiplayerGameDTO.GameToGameDTO(game));
            }           
           
            if (game.Finished)
            {
                _gameService.RemoveGameById(game.Id);
                _logger.LogInformation($"Multiplayer game with id '{game.Id}' has finished and is removed from the running games list.");
            }
        }

        /// <summary>
        /// Runs when client sends request to set his name
        /// </summary>
        public async Task SetName(string name)
        {
            var connectionId = Context.ConnectionId;
            var game = _gameService.GetAllGames().FirstOrDefault(game => game.Player1.ConnectionId == connectionId || game.Player2.ConnectionId == connectionId);
            var player = game.Player1.ConnectionId == connectionId ? game.Player1 : game.Player2;

            if (player != null)
            {
                player.Name = name;
            }
        }

        /// <summary>
        /// Runs when a user connects to the server. Creates a new game or joins an existing game and adds the user to it.
        /// </summary>
        public override async Task OnConnectedAsync()
        {            
            var connectionId = Context.ConnectionId;
            var game = _gameService.GetAllGames().FirstOrDefault(game => !game.InProgress);
            if(game == null)
            {
                ///No one is looking for a game yet, create a new one
                var player1 = new Player(connectionId);
                var gameId = _gameService.CreateNewGame(player1);
                game = _gameService.GetGameById(gameId);
                _logger.LogInformation($"New multiplayer game with id {gameId} is created by player with connectionId {connectionId}. Looking for second player...");
            }
            else
            {
                ///Existing game found, join the game
                var player2 = new Player(connectionId);
                var succes = game.AddPlayer2(player2);
                if (succes)
                {
                    _logger.LogInformation($"Player2 with connectionId {connectionId} joined an existing game with gameId {game.Id}");
                }
                else
                {
                    _logger.LogCritical($"Existing game was found, but something went wrong trying to join it (maybe someone else joined it before??)");
                }
            }
            await Groups.AddToGroupAsync(connectionId, game.Id.ToString());
            await base.OnConnectedAsync();

            if (game.InProgress)
            {
                await Clients.Group(game.Id.ToString()).SendNewBoardState(MultiplayerGameDTO.GameToGameDTO(game));
            }
        }

        /// <summary>
        /// Runs when user disconnects from the server. Removes the game from running games list and notifies other player that player has left.
        /// </summary>
        public override async Task OnDisconnectedAsync(Exception exception)
        {
            var connectionId = Context.ConnectionId;
            var game = _gameService.GetAllGames().FirstOrDefault(game => game.Player1.ConnectionId == connectionId || game.Player2.ConnectionId == connectionId);

            if(game != null)
            {
                ///Player disconnected from game, remove the game from running games
                await Groups.RemoveFromGroupAsync(connectionId, game.Id.ToString());
                await Clients.Group(game.Id.ToString()).SendPlayerDisconnected();
                _gameService.RemoveGameById(game.Id);
                _logger.LogInformation($"Game with id {game.Id} removed from running games list");
            }

            await base.OnDisconnectedAsync(exception);
        }
    }
}
