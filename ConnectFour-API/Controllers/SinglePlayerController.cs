﻿namespace ConnectFour_Core.Controllers
{
    using System;
    using System.Collections.Generic;
    using ConnectFour_Core.Models;
    using ConnectFour_Core.Models.DTO;
    using ConnectFour_Core.Services.Interfaces;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    [ApiController]
    [Route("[controller]")]
    public class SinglePlayerController : ControllerBase
    {
        private readonly ILogger<SinglePlayerController> _logger;
        private readonly IConnectFourService _connectFourService;
        private readonly IGameService _gameService;

        public SinglePlayerController(ILogger<SinglePlayerController> logger, IConnectFourService connectFourService, IGameService gameService)
        {
            _logger = logger;
            _connectFourService = connectFourService;
            _gameService = gameService;
        }

        //Make move 
        ///TODO: Add verification, like bearer tokens
        [HttpGet("{gameId}/{columnIndex}")]
        public SingleplayerGameDTO AddChip(Guid gameId, byte columnIndex)
        {
            var newGameState = _connectFourService.AddChip(gameId, columnIndex);

            var gameDTO = SingleplayerGameDTO.GameToGameDTO(newGameState);

            //Game finished, remove from list
            if (gameDTO.Finished)
            {
                _gameService.RemoveGameById(gameId);
                _logger.LogInformation($"Game with id '{gameId}' has finished and is removed from the running games list.");
            }
            return gameDTO;
        }

        //Create game (temp) 
        ///TODO: Add queue so people can search for games and connect to one
        [HttpGet("newgame/{player1Name}/{player2Name}")]
        public SingleplayerGameDTO CreateNewGame(string player1Name, string player2Name)
        {
            if (string.IsNullOrEmpty(player1Name.Trim()))
            {
                player1Name = "Player 1";
            }
            if (string.IsNullOrEmpty(player1Name.Trim()))
            {
                player2Name = "Player 2";
            }

            var players = new List<Player>()
            {
                new Player(false, player1Name),
                new Player(true, player2Name)
            };

            var gameId = _gameService.CreateNewGame(players);
            var game = _gameService.GetGameById(gameId);
            var gameDTO = SingleplayerGameDTO.GameToGameDTO(game);

            _logger.LogInformation($"Game with id '{gameId}' has been created");

            return gameDTO;
        }
    }
}
