﻿using ConnectFour_Core.Extensions;
using NUnit.Framework;

namespace ConnectFour_Test
{
    public class ExtensionTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void WithNullableBoolExtension_AssertReturnsSameValue()
        {
            bool? trueFalseNull = null;
            bool? newValue = trueFalseNull.GetValueOrNull();
            Assert.AreEqual(null, newValue, "A nullable bool with 'null' as value should return null");

            trueFalseNull = true;
            newValue = trueFalseNull.GetValueOrNull();
            Assert.AreEqual(true, newValue, "A nullable bool with 'true' as value should return true");

            trueFalseNull = false;
            newValue = trueFalseNull.GetValueOrNull();
            Assert.AreEqual(false, newValue, "A nullable bool with 'false' as value should return false");
        }
    }
}
