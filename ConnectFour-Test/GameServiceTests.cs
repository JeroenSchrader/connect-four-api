﻿namespace ConnectFour_Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ConnectFour_Core.Models;
    using ConnectFour_Core.Services;
    using ConnectFour_Core.Services.Interfaces;
    using NUnit.Framework;

    public class GameServiceTests
    {
        private IGameService _gameService;

        [SetUp]
        public void Setup()
        {
            _gameService = new GameService();
        }

        [Test]
        public void CreateGetRemoveGame_AssertValidState()
        {
            var players = new List<Player>
            {
                new Player(false, "Test1"),
                new Player(true, "Test2")
            };

            var gameId = _gameService.CreateNewGame(players);
            Assert.IsNotNull(gameId, "GameId (GUID) should not be null");
            var game = _gameService.GetGameById(gameId);
            Assert.IsNotNull(game, "Game should not be null");
            Assert.AreEqual(1, _gameService.GetAllRunningGames().Count, "There should only be 1 game running");
            Assert.AreEqual(game, _gameService.GetAllRunningGames().First(game => game.Id == gameId), "Game should be in the list of all games");

            Assert.AreEqual(false, _gameService.RemoveGameById(Guid.NewGuid()), "Game should not be found");
            Assert.AreEqual(true, _gameService.RemoveGameById(gameId), "Game should be correctly removed");
            Assert.AreEqual(0, _gameService.GetAllRunningGames().Count, "There should currently be no games running");
        }
    }
}
