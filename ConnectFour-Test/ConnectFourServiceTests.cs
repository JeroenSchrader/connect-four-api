﻿namespace ConnectFour_Test
{
    using System;
    using System.Collections.Generic;
    using ConnectFour_Core.Models;
    using ConnectFour_Core.Services;
    using ConnectFour_Core.Services.Interfaces;
    using Microsoft.Extensions.Logging;
    using Moq;
    using NUnit.Framework;

    public class ConnectFourServiceTests
    {
        private List<Player> _players;
        private Guid _testGuid;
        private Game _testGame;

        private IConnectFourService _connectFourService;
        private IGameService _gameService;

        [SetUp]
        public void Setup()
        {
            _players = new List<Player>
            {
                new Player(false, "Player 1"),
                new Player(true, "Player 2")
            };
            _testGuid = Guid.NewGuid();
            _testGame = new Game(_players);
        }

        [Test]
        public void WithConnectFourService_AssertCreatingGameWorks()
        {
            var mockGameService = new Mock<IGameService>();
            mockGameService.Setup(service => service.CreateNewGame(_players)).Returns(_testGuid);
            _gameService = mockGameService.Object;

            var mockLogger = new Mock<ILogger<ConnectFourService>>();
            _connectFourService = new ConnectFourService(_gameService, mockLogger.Object);

            Assert.Throws<ArgumentException>(() => _connectFourService.AddChip(_testGuid, 0), $"Game with Id {_testGuid} should not exists at the moment");
            Assert.AreEqual(_testGuid, _gameService.CreateNewGame(_players), "Guid is not as expected");
            Assert.Throws<ArgumentException>(() => _connectFourService.AddChip(_testGuid, 254), "Column 254 should be too big for the game board");
        }

        [Test]
        public void WithConnectFourService_AssertAddingChipsWorks()
        {
            var mockGameService = new Mock<IGameService>();
            mockGameService.Setup(service => service.GetGameById(_testGuid)).Returns(_testGame);
            _gameService = mockGameService.Object;

            var mockLogger = new Mock<ILogger<ConnectFourService>>();
            _connectFourService = new ConnectFourService(_gameService, mockLogger.Object);

            Assert.AreEqual(_testGame, _connectFourService.AddChip(_testGuid, 254), "Column 254 should be too big for the game board and thus it should just return the current board");
            Assert.DoesNotThrow(() => _connectFourService.AddChip(_testGuid, 0), "Adding a chip to column index 0 should work in a new game");
            Assert.AreEqual(_testGame.Board.Grid[5, 0].Value, false, "Chip added in first column (index 0) should belong to player 1 (0/false)");
            Assert.AreNotEqual(_testGame.Board.Grid[5, 0].Value, true, "Chip added in first column (index 0) should belong to player 1 (0/false)");
            Assert.DoesNotThrow(() => _connectFourService.AddChip(_testGuid, 0), "Adding a chip to column index 0 should work in a new game");
            Assert.AreEqual(_testGame.Board.Grid[4, 0].Value, true, "The second chip added in first column (index 0) should belong to player 2 (1/true)");
            Assert.AreNotEqual(_testGame.Board.Grid[4, 0].Value, false, "The second chip added in first column (index 0) should belong to player 2 (1/true)");
        }
    }
}
