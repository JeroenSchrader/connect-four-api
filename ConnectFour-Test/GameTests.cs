﻿using System.Collections.Generic;
using ConnectFour_Core.Models;
using NUnit.Framework;

namespace ConnectFour_Test
{
    public class GameTests
    {
        private List<Player> _players;
        private Game _game;

        [SetUp]
        public void Setup()
        {
            _players = new List<Player>
            {
                new Player(false, "Player 1"),
                new Player(true, "Player 2")
            };

            _game = new Game(_players);
        }

        [Test]
        public void WithNewGame_AssertStateIsValid()
        {
            Assert.AreEqual(_players, _game.Players, "Players should be equal to the passed players when game is created");
            Assert.AreEqual(_players[0], _game.CurrentPlayerTurn, "Player 1 should be the first player to move");
            Assert.AreEqual(false, _game.Finished, "Newly created game should not be finished");
            Assert.AreEqual(null, _game.Winner, "Newly created game should not have a winner");

            Assert.IsTrue(_game.AddChip(0), "Adding a chip to the first column in a new game should be a valid move");
            Assert.AreEqual(false, _game.Board.Grid[5, 0].Value, "Chip at 5,0 should belong to Player 1 (0/false)");
            Assert.AreEqual(_players[1], _game.CurrentPlayerTurn, "Player 2 should be next to move");
            Assert.IsTrue(_game.AddChip(1));
            Assert.AreEqual(_players[0], _game.CurrentPlayerTurn, "Player 1 should be next to move");
            Assert.IsTrue(_game.AddChip(0));
            Assert.IsTrue(_game.AddChip(1));
            Assert.IsTrue(_game.AddChip(0));
            Assert.IsTrue(_game.AddChip(1));
            Assert.IsTrue(_game.AddChip(0));

            Assert.IsTrue(_game.Finished, "Player 1 should have won and thus game has finished");
            Assert.AreEqual(_players[0], _game.Winner, "Player 1 should be the winner");
            Assert.IsNull(_game.CurrentPlayerTurn, "There should be no current player");
        }
    }
}
