namespace ConnectFour_Test
{
    using System;
    using ConnectFour_Core.Models;
    using NUnit.Framework;

    public class BoardTests
    {
        private Board _board;

        [SetUp]
        public void Setup()
        {
            _board = new Board();
        }

        [Test]
        public void CreateNewBoardWithInvalidDimensions_AssertThrowsException()
        {
            Assert.DoesNotThrow(() => new Board(15, 15), "15/15 board should be valid");
            Assert.DoesNotThrow(() => new Board(6, 6), "6/6 board should be valid");
            Assert.DoesNotThrow(() => new Board(6, 15), "6/15 board should be valid");
            Assert.DoesNotThrow(() => new Board(15, 6), "15/6 board should be valid");
            Assert.Throws<ArgumentException>(() => new Board(16, 6), "16/6 board should not be valid, maximum width/height of 15 is allowed");
            Assert.Throws<ArgumentException>(() => new Board(1, 6), "1/6 board should not be valid, minimum width/height of 6 is required");
        }

        [Test]
        public void WithNewlyCreatedBoard_AssertBoardStatesAreValid()
        {
            //Check initial board state
            Assert.IsFalse(_board.IsFull, "Board should not be full when it is just created");
            Assert.AreEqual(42, _board.Grid.Length, "Standard board should contain 42 items (6*7 dimension)");
            Assert.AreEqual(6, _board.Grid.GetLength(0), "Standard board should be 6 cells high (height)");
            Assert.AreEqual(7, _board.Grid.GetLength(1), "Standard board should be 7 cells wide (width)");
            Assert.AreEqual(null, _board.GetWinner(), "There should be no winner when a new board is created");
            foreach (var cell in _board.Grid)
            {
                Assert.IsFalse(cell.HasValue, "Cell within a newly created board should be null (no player put a chip on it yet)");
            }

            //Add chip and check state again
            Assert.IsTrue(_board.AddChip(false, 0), "Chip should be succesfully added");
            Assert.IsTrue(_board.AddChip(true, 0), "Chip should be succesfully added");
            Assert.IsTrue(_board.AddChip(false, 0), "Chip should be succesfully added");
            Assert.AreEqual(false, _board.Grid[5, 0], "Chip should be within the correct cell");
            Assert.AreEqual(true, _board.Grid[4, 0], "Chip should be within the correct cell");
            Assert.AreEqual(false, _board.Grid[3, 0], "Chip should be within the correct cell");

            Assert.IsTrue(_board.AddChip(true, 1), "Chip should be succesfully added");
            Assert.IsTrue(_board.AddChip(false, 2), "Chip should be succesfully added");
            Assert.IsTrue(_board.AddChip(true, 3), "Chip should be succesfully added");
            Assert.AreEqual(true, _board.Grid[5, 1], "Chip should be within the correct cell");
            Assert.AreEqual(false, _board.Grid[5, 2], "Chip should be within the correct cell");
            Assert.AreEqual(true, _board.Grid[5, 3], "Chip should be within the correct cell");
        }

        [Test]
        public void WithNewlyCreatedBoard_AssertBoardBecomesFull()
        {
            //Fill the board
            for (byte i = 0; i < 6; i++)
            {
                for (byte j = 0; j < 7; j++)
                {
                    _board.AddChip(true, j);
                }
            }

            Assert.IsTrue(_board.IsFull);

            Assert.AreEqual(false, _board.AddChip(true, 0), "Chip should not be added as board is already full");
        }

        [Test]
        public void WithBoardThatContainsWinner_AssertCorrectPlayerWon()
        {
            //Vertical check
            Assert.IsTrue(_board.AddChip(false, 0), "Chip should be succesfully added");
            Assert.IsTrue(_board.AddChip(false, 0), "Chip should be succesfully added");
            Assert.IsTrue(_board.AddChip(false, 0), "Chip should be succesfully added");
            Assert.IsTrue(_board.AddChip(false, 1), "Chip should be succesfully added");
            Assert.IsTrue(_board.AddChip(false, 1), "Chip should be succesfully added");
            Assert.IsTrue(_board.AddChip(false, 1), "Chip should be succesfully added");
            Assert.AreEqual(null, _board.GetWinner(), "There should be no winner yet");
            Assert.IsTrue(_board.AddChip(false, 0), "Chip should be succesfully added");
            Assert.AreEqual(false, _board.GetWinner(), "Player 1 should be the winner");

            _board.Reset();

            //Horizontal check
            Assert.IsTrue(_board.AddChip(false, 0), "Chip should be succesfully added");
            Assert.IsTrue(_board.AddChip(false, 1), "Chip should be succesfully added");
            Assert.IsTrue(_board.AddChip(false, 2), "Chip should be succesfully added");
            Assert.AreEqual(null, _board.GetWinner(), "There should be no winner yet");
            Assert.IsTrue(_board.AddChip(false, 3), "Chip should be succesfully added");
            Assert.AreEqual(false, _board.GetWinner(), "Player 1 should be the winner");

            _board.Reset();

            //Diagonal up check
            Assert.IsTrue(_board.AddChip(false, 0), "Chip should be succesfully added");
            Assert.IsTrue(_board.AddChip(true, 1), "Chip should be succesfully added");
            Assert.IsTrue(_board.AddChip(true, 2), "Chip should be succesfully added");
            Assert.IsTrue(_board.AddChip(true, 3), "Chip should be succesfully added");
            Assert.IsTrue(_board.AddChip(false, 1), "Chip should be succesfully added");
            Assert.IsTrue(_board.AddChip(false, 2), "Chip should be succesfully added");
            Assert.IsTrue(_board.AddChip(false, 2), "Chip should be succesfully added");
            Assert.AreEqual(null, _board.GetWinner(), "There should be no winner yet");
            Assert.IsTrue(_board.AddChip(true, 3), "Chip should be succesfully added");
            Assert.IsTrue(_board.AddChip(true, 5), "Chip should be succesfully added");
            Assert.IsTrue(_board.AddChip(false, 3), "Chip should be succesfully added");
            Assert.IsTrue(_board.AddChip(false, 3), "Chip should be succesfully added");
            Assert.AreEqual(false, _board.GetWinner(), "Player 1 should be the winner");

            _board.Reset();

            //Diagonal down check
            Assert.IsTrue(_board.AddChip(true, 0), "Chip should be succesfully added");
            Assert.IsTrue(_board.AddChip(true, 0), "Chip should be succesfully added");
            Assert.IsTrue(_board.AddChip(true, 0), "Chip should be succesfully added");
            Assert.IsTrue(_board.AddChip(false, 0), "Chip should be succesfully added");
            Assert.IsTrue(_board.AddChip(true, 1), "Chip should be succesfully added");
            Assert.IsTrue(_board.AddChip(true, 1), "Chip should be succesfully added");
            Assert.IsTrue(_board.AddChip(false, 1), "Chip should be succesfully added");
            Assert.IsTrue(_board.AddChip(true, 2), "Chip should be succesfully added");
            Assert.IsTrue(_board.AddChip(false, 2), "Chip should be succesfully added");
            Assert.AreEqual(null, _board.GetWinner(), "There should be no winner yet");
            Assert.IsTrue(_board.AddChip(false, 3), "Chip should be succesfully added");
            Assert.AreEqual(false, _board.GetWinner(), "Player 1 should be the winner");
        }
    }
}